package br.com.rodrigo;

import java.util.Scanner;

public class Calculadora {

    private String nome;
    
    public String faixaMaisVelha() {

        Pessoa p1 = new Pessoa(38, "Marcos");
        Pessoa p2 = new Pessoa(35, "Rodrigo");
        Pessoa p3 = new Pessoa(28, "Alain");

        if (p1.getIdade() > p2.getIdade() && p1.getIdade() > p3.getIdade()) {
            nome = p1.getNome();

            return nome;
        }

        if (p2.getIdade()
                > p1.getIdade() && p2.getIdade() > p3.getIdade()) {
            nome = p2.getNome();

            return nome;
        }

        if (p3.getIdade()
                > p1.getIdade() && p3.getIdade() > p2.getIdade()) {
            nome = p3.getNome();
        }

        return nome;

    }

    public String faixaMaisNova() {

        Pessoa p1 = new Pessoa(38, "Marcos");
        Pessoa p2 = new Pessoa(35, "Rodrigo");
        Pessoa p3 = new Pessoa(28, "Alain");

        if (p1.getIdade() < p2.getIdade() && p1.getIdade() < p3.getIdade()) {
            nome = p1.getNome();

            return nome;
        }

        if (p2.getIdade() < p1.getIdade() && p2.getIdade() < p3.getIdade()) {
            nome = p2.getNome();

            return nome;
        }

        if (p3.getIdade() < p1.getIdade() && p3.getIdade() < p2.getIdade()) {
            nome = p3.getNome();
        }

        return nome;

    }
}
