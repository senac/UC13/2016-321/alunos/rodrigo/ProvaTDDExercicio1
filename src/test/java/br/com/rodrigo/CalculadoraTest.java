
package br.com.rodrigo;

import br.com.rodrigo.Calculadora;
import br.com.rodrigo.Pessoa;
import org.junit.Test;
import static org.junit.Assert.*;


public class CalculadoraTest {
    
    @Test
    public void deveMostarAPessoaMaisVelha(){
        Calculadora calculadora = new Calculadora();
       
            
        String resultado = "Marcos";
        assertEquals(calculadora.faixaMaisVelha(), resultado);
        
    }
    

 @Test
    public void deveMostarAPessoaMaisNova(){
        Calculadora calculadora = new Calculadora();
        Pessoa pessoa = new Pessoa();
        
        String resultado = "Alain";
        assertEquals(calculadora.faixaMaisNova(), resultado);
        
    }
    
}
